import  unittest
from calculadora import suma,resta,multiplicacion,division,sqrt


class TestOperacionesSuma(unittest.TestCase):
    # Aquí vamos a probar los números naturales
    def test_suma1(self):
        valores_pruebas = ((1,2,3), (3,2,5), (10,10,20), (33,33,66), (90,9,99))
        lista_resultados = []
        lista_esperados = []

        for tupla in valores_pruebas:
            a = tupla[0]
            b = tupla[1]
            lista_resultados.append(suma(a,b))
            lista_esperados.append(tupla[2])

        self.assertListEqual(lista_resultados,lista_esperados)

    #Aquí vamos a probar los números enteros
    def test_suma2(self):
        valores_pruebas = ((-1,2,1), (2,-3,-1), (-3,2,-1), (2,-1,1), (-100,-2,-102))
        lista_resultados = []
        lista_esperados = []

        for tupla in valores_pruebas:
            a = tupla[0]
            b = tupla[1]
            lista_resultados.append(suma(a,b))
            lista_esperados.append(tupla[2])

        self.assertListEqual(lista_resultados,lista_esperados)

    #Lo anterior(enteros) más reales
    def test_suma3(self):
        valores_pruebas = ((-1,100,99), (-2,8,6), (3.5,1,4.5), (0,-0.1,-0.1),
        (0,3,3), (0,0,0))
        lista_resultados = []
        lista_esperados = []

        for tupla in valores_pruebas:
            a = tupla[0]
            b = tupla[1]
            lista_resultados.append(suma(a,b))
            lista_esperados.append(tupla[2])

        self.assertListEqual(lista_resultados,lista_esperados)

class TestOperacionesResta(unittest.TestCase):
    # Aquí vamos a probar los números naturales
    def test_resta1(self):
        valores_pruebas = ((1,2,-1), (3,2,1), (10,10,0), (33,33,0), (90,9,81))
        lista_resultados = []
        lista_esperados = []

        for tupla in valores_pruebas:
            a = tupla[0]
            b = tupla[1]
            lista_resultados.append(resta(a,b))
            lista_esperados.append(tupla[2])

        self.assertListEqual(lista_resultados,lista_esperados)

    #Aquí vamos a probar los números enteros
    def test_resta2(self):
        valores_pruebas = ((-1,2,-3), (2,-3,5), (-3,2,-5), (2,-1,3), (-100,-2,-98))
        lista_resultados = []
        lista_esperados = []

        for tupla in valores_pruebas:
            a = tupla[0]
            b = tupla[1]
            lista_resultados.append(resta(a,b))
            lista_esperados.append(tupla[2])

        self.assertListEqual(lista_resultados,lista_esperados)

    #Lo anterior(enteros) más reales
    def test_resta3(self):
        valores_pruebas = ((-1,100,-101), (-2,8,-10), (3.5,1,2.5), (0,-0.1,0.1),
        (0,3,-3), (0,0,0))
        lista_resultados = []
        lista_esperados = []

        for tupla in valores_pruebas:
            a = tupla[0]
            b = tupla[1]
            lista_resultados.append(resta(a,b))
            lista_esperados.append(tupla[2])

        self.assertListEqual(lista_resultados,lista_esperados)

class TestOperacionesMultiplicacion(unittest.TestCase):
    # Aquí vamos a probar los números naturales
    def test_multiplicacion1(self):
        valores_pruebas = ((1,2,2), (3,2,6), (10,10,100), (33,33,1089), (90,9,810))
        lista_resultados = []
        lista_esperados = []

        for tupla in valores_pruebas:
            a = tupla[0]
            b = tupla[1]
            lista_resultados.append(multiplicacion(a,b))
            lista_esperados.append(tupla[2])

        self.assertListEqual(lista_resultados,lista_esperados)

    #Aquí vamos a probar los números enteros
    def test_multiplicacion2(self):
        valores_pruebas = ((-1,2,-2), (2,-3,-6), (-3,2,-6), (2,-1,-2), (-100,-2,200))
        lista_resultados = []
        lista_esperados = []

        for tupla in valores_pruebas:
            a = tupla[0]
            b = tupla[1]
            lista_resultados.append(multiplicacion(a,b))
            lista_esperados.append(tupla[2])

        self.assertListEqual(lista_resultados,lista_esperados)

    #Lo anterior(enteros) más reales
    def test_multiplicacion3(self):
        valores_pruebas = ((-1,100,-100), (-2,8,-16), (3.5,1,3.5), (0,-0.1,0),
        (0,3,0), (0,0,0))
        lista_resultados = []
        lista_esperados = []

        for tupla in valores_pruebas:
            a = tupla[0]
            b = tupla[1]
            lista_resultados.append(multiplicacion(a,b))
            lista_esperados.append(tupla[2])

        self.assertListEqual(lista_resultados,lista_esperados)

class TestOperacionesDivision(unittest.TestCase):
    # Aquí vamos a probar los números naturales
    def test_division1(self):
        valores_pruebas = ((1,2,0.5), (3,2,1.5), (10,10,1), (33,33,1), (90,9,10))
        lista_resultados = []
        lista_esperados = []

        for tupla in valores_pruebas:
            a = tupla[0]
            b = tupla[1]
            lista_resultados.append(division(a,b))
            lista_esperados.append(tupla[2])

        self.assertListEqual(lista_resultados,lista_esperados)

    #Aquí vamos a probar los números enteros
    def test_division2(self):
        valores_pruebas = ((-1,2,-0.5), (2,-3,-0.66667), (-3,2,-1.5), (2,-1,-2), (-100,-2,50))
        lista_resultados = []
        lista_esperados = []

        for tupla in valores_pruebas:
            a = tupla[0]
            b = tupla[1]
            lista_resultados.append(division(a,b))
            lista_esperados.append(tupla[2])

        #Comprobamos que la lista de resultados tiene valores.
        self.assertNotEqual(0, len(lista_resultados),"No se ha computado ningun resultado")

        for i in range(0,len(lista_resultados)):
           resultadoReal = lista_resultados[i]
           resultadoEsperado = lista_esperados[i]

           a = valores_pruebas[i][0]
           b = valores_pruebas[i][1]

           mensajeError = f"El valor es distinto en el {i+1} elemento. {a} / {b} !={resultadoReal}"
           self.assertAlmostEqual(resultadoReal,resultadoEsperado,5,mensajeError)

    #Lo anterior(enteros) más reales
    def test_division3(self):
        valores_pruebas = ((-1,100,-0.01), (-2,8,-0.25), (3.5,1,3.5), (0,-0.1,0),
        (0,3,0), (0,1,0))
        lista_resultados = []
        lista_esperados = []

        for tupla in valores_pruebas:
            a = tupla[0]
            b = tupla[1]
            lista_resultados.append(division(a,b))
            lista_esperados.append(tupla[2])

        self.assertListEqual(lista_resultados,lista_esperados)

class TestOperacionesRaizCuadrada(unittest.TestCase):

   def test_sqrt1(self):
       valores_prueba = ( (4,2) , (2,1.41421), (16,4) , (5,2.23607), (1,1))
       lista_resultados = []
       lista_esperados = []

       for tupla in valores_prueba:
          a = tupla[0]
          lista_resultados.append(sqrt(a))
          lista_esperados.append(tupla[1])

        #Comprobamos que la lista de resultados tiene valores.
       self.assertNotEqual(0, len(lista_resultados),"No se ha computado ningun resultado")

       for i in range(0,len(lista_resultados)):
           resultadoReal = lista_resultados[i]
           resultadoEsperado = lista_esperados[i]

           a=valores_prueba[i][0]

           mensajeError = f"El valor es distinto en el {i+1} elemento. Sqrt({a})!={resultadoEsperado}"
           self.assertAlmostEqual(resultadoReal,resultadoEsperado,5,mensajeError)





if __name__ == "__main__":
    unittest.main()
