def suma(a,b):
    return a+b

def resta(a,b):
    return a-b

def multiplicacion(a,b):
   return a*b

def division(a,b):
   return a/b

def sqrt(n):
   inferior = 0
   superior = float(n)

   for i in range(0,1000):
      mitad = (inferior + superior)/2
      mitadPower = mitad * mitad
      #Hemos encontrado la raiz exacta
      if(mitadPower == n ): return mitad
      if(mitadPower > n ): superior = mitad
      else:
         inferior = mitad

   #No hemos encontrado una raiz adecuada, devolvemos la mejor que habiamos
   #encontrado
   return mitad
